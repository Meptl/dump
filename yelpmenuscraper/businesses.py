import requests
import os
import subprocess

ENDPOINT = "https://api.yelp.com/v3/graphql"
API_KEY = os.environ['YELP_API_KEY']
CHECKPOINT_FILE = 'checkpoint'
LOC_FILE = 'locs'
OUTPUT_FILE = 'businesses'

def save_progress(loc, offset):
    with open(CHECKPOINT_FILE, 'w') as f:
        f.write(f'{loc};{offset}')
    print(loc, offset)


def query_businesses(loc, offset=0):
    print(f'Querying {loc} with {offset} offset')
    body = """{{
      search(location: "{loc}",
        categories: "Chinese", term:"Chinese", limit: 50, offset: {offset}) {{
        total
        business {{
          alias
        }}
      }}
    }}""".format(loc=loc, offset=offset)
    res = requests.post(
        url=ENDPOINT,
        headers={'Authorization': f'Bearer {API_KEY}', 'Accept-Language': 'en_US'},
        json={"query": body})
    if res.status_code != 200:
        raise Exception(f'Request error: {res.status_code}\nContent: {res.content}')
    return res.json()


def businesses(loc, offset=0):
    try:
        aliases = []
        res = query_businesses(loc)
        total = res['data']['search']['total']

        # Yelp has an internal error for anything above 1000.
        while offset < total and offset < 1000:
            res = query_businesses(loc, offset=offset)
            for biz in res['data']['search']['business']:
                aliases.append(biz["alias"])
            offset += 50
    except Exception as e:
        if hasattr(e, 'message'):
            print(e.message)
        else:
            print(e)
        save_progress(loc, offset)
        raise e

    return aliases


def main():
    skip_to_loc = None
    skip_to_offset = 0
    if os.path.exists(CHECKPOINT_FILE):
        with open(CHECKPOINT_FILE, 'r') as checkpointf:
            skip_to_loc, skip_to_offset = checkpointf.read().split(';')
            skip_to_loc.strip()
            skip_to_offset = int(skip_to_offset.strip())

    with open(LOC_FILE, 'r') as locf, open(OUTPUT_FILE, 'a') as outf:
        for loc in locf.readlines():
            loc = loc.strip()
            if skip_to_loc:
                if loc != skip_to_loc:
                    print('skipping')
                    continue
                else:
                    skip_to_loc = None
            aliases = businesses(loc, skip_to_offset)
            skip_to_offset = 0
            outf.write("\n".join(aliases) + "\n")

    subprocess.run(f'sort --unique {OUTPUT_FILE} -o {OUTPUT_FILE}', shell=True);


if __name__ == '__main__':
    main()
