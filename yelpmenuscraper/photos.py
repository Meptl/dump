import requests
import os
from bs4 import BeautifulSoup

TOP_LEVEL_URL = "https://www.yelp.com"
BUSINESS_FILE = 'businesses'
CHECKPOINT_FILE = 'checkpoint'
OUTPUT_FILE = 'urls'
OUTPUT_FOLDER = 'images'


def save_progress(loc):
    with open(CHECKPOINT_FILE, 'w') as f:
        f.write(f'{loc}')


def collect_photo_urls(alias, outf):
    menu = f'{TOP_LEVEL_URL}/menu/{alias}'
    print(f'Getting menu for {alias}')
    res = requests.get(menu, allow_redirects=False)
    if res.status_code != 200:
        if 'captcha' in res.headers['location']:
            raise Exception('Received captcha.')
        else:
            print('No menu')
            return

    soup = BeautifulSoup(res.content, features="html.parser")
    photo_links = [a.get('href') for a in soup.find_all('a') if 'photo' in a.text]
    for link in photo_links:
        menu_item = link.split('/')[-1]
        urls = scrape_photo_page(f'{TOP_LEVEL_URL}/biz_photos/{alias}?menu-item={menu_item}')
        for i, url in enumerate(urls):
            img_data = requests.get(url)

            outf = f'{OUTPUT_FOLDER}/{alias}-{menu_item}-{i:02}.jpg'
            print(f'Saving to {outf}')
            with open(outf, 'wb') as f:
                f.write(img_data.content)


def scrape_photo_page(url):
    print(f'Getting at most 3 photos from {url}')
    res = requests.get(url)
    soup = BeautifulSoup(res.content, features="html.parser")
    return [i.get('src') for i in soup.find_all('img', width='226', limit=3)]


def main():
    skip_to_loc = None
    if os.path.exists(CHECKPOINT_FILE):
        with open(CHECKPOINT_FILE, 'r') as checkpointf:
            skip_to_loc = checkpointf.read().strip()

    with open(BUSINESS_FILE, 'r') as f, open(OUTPUT_FILE, 'a') as outf:
        for alias in f.readlines():
            alias = alias.strip()
            if skip_to_loc:
                if alias != skip_to_loc:
                    print(f'Skipping {alias}')
                    continue
                else:
                    skip_to_loc = None

#             found = False
#             for im in images:
#                 if im.startswith(alias):
#                     found = True
#                     break

#             if not found:
#                 print(alias)

            try:
                collect_photo_urls(alias, outf)
            except Exception as e:
                if hasattr(e, 'message'):
                    print(e.message)
                else:
                    print(e)
                raise e
            finally:
                save_progress(alias)


if __name__ == '__main__':
    main()
