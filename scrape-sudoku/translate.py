import uuid
from sudoku import Sudoku

def arr2str(arr):
    s = ''
    for row in arr:
        for cell in row:
            if cell == None:
                s += "."
            else:
                s += str(cell)
    return s


def str2arr(str):
    if len(str) != 81:
        print("FUUU")
    arr = []
    for i in range(9):
        x = []
        for j in range(9):
            val = str[i * 9 + j]
            if val == '.':
                x.append(0)
            else:
                x.append(int(val))
        arr.append(x)
    return arr;


def solve_sudoku_file(input_file):
    with open(input_file, 'r') as file:
        lines = file.readlines()

    for line in lines:
        line = line.strip()

        # Create a Sudoku object from the puzzle specification
        puz = line.replace('.','0')
        puzzle = Sudoku(3, 3, str2arr(puz))
        solution = puzzle.solve()

        # Generate a unique filename using UUID
        filename = "output/" + str(uuid.uuid4()) + '.txt'

        with open(filename, 'w') as output_file:
            output_file.write(line + '\n')
            output_file.write(arr2str(solution.board) + '\n')


# Usage example:
input_filename = 'very-hard-puzzles'
solve_sudoku_file(input_filename)
