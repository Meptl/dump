import argparse
import random
import sys

import numpy as np
import pyaudio
import pygame

def get_mic_intensity(stream, chunk):
    data = np.frombuffer(stream.read(chunk), dtype=np.int16)
    return np.max(data)


def main(height_ratio, background_color, mic_min_threshold):
    # Load images
    base_image = pygame.image.load("base.png")
    base_blink_image = pygame.image.load("base_blink.png")
    talk_image = pygame.image.load("talk.png")
    talk_blink_image = pygame.image.load("talk_blink.png")

    # Init microphone
    chunk = 1024
    p = pyaudio.PyAudio()
    stream = p.open(format=pyaudio.paInt16, channels=1, rate=44100, input=True, frames_per_buffer=chunk)

    # Init window.
    pygame.init()
    pygame.display.set_caption('pngtuber')
    clock = pygame.time.Clock()
    image_width, image_height = base_image.get_size()
    window_width = image_width
    window_height = int(height_ratio * image_height)
    screen = pygame.display.set_mode((window_width, window_height))
    base_image = base_image.convert_alpha()
    base_blink_image = base_blink_image.convert_alpha()
    talk_image = talk_image.convert_alpha()
    talk_blink_image = talk_blink_image.convert_alpha()

    ease_ratio = 0.1
    base_y_position = window_height - image_height
    y_position = base_y_position
    prev_y_position = base_y_position
    running = True
    last_mic_time = 0
    current_image = base_image
    current_blink_image = base_blink_image

    blink_timer = 0
    blink_interval = random.randint(3000, 5000)

    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False

        intensity = get_mic_intensity(stream, chunk)
        current_time = pygame.time.get_ticks()

        # Calculate desired y_position this is reset each frame.
        if intensity > mic_min_threshold:
            y_position = 0.4 * base_y_position
            clip_val = 15000
            intensity_norm_clip = np.clip(intensity, 0, clip_val) / clip_val
            y_position -= 0.4 * (base_y_position) * intensity_norm_clip
            last_mic_time = pygame.time.get_ticks()
            current_image = talk_image
            current_blink_image = talk_blink_image
        elif pygame.time.get_ticks() - last_mic_time > 100:
            y_position = base_y_position
            if pygame.time.get_ticks() - last_mic_time > 260:
                current_image = base_image
                current_blink_image = base_blink_image
            else:
                current_image = talk_image
                current_blink_image = talk_blink_image

        # Blink logic
        if current_time > blink_timer + blink_interval:
            blink_timer = current_time
            blink_interval = random.randint(3000, 5000)

        y_position = ease_ratio * y_position + (1 - ease_ratio) * prev_y_position
        prev_y_position = y_position

        screen.fill(background_color)

        if blink_timer == current_time:
            screen.blit(current_blink_image, (0, int(y_position)))
        else:
            screen.blit(current_image, (0, int(y_position)))

        pygame.display.flip()
        clock.tick(30)

    stream.stop_stream()
    stream.close()
    p.terminate()
    pygame.quit()
    sys.exit()

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--height-ratio", type=float, default=1.1)
    parser.add_argument("--background-color", nargs=3, type=int, metavar=("R", "G", "B"), default=(0, 255, 0))
    parser.add_argument("--mic-min-threshold", type=int, default=1400)
    args = parser.parse_args()
    main(args.height_ratio, tuple(args.background_color), args.mic_min_threshold)
