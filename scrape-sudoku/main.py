# Import the necessary modules
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.by import By


def print_sudoku(driver, outf):
    # Find the "table" element with class "Grid"
    grid_table = driver.find_element(By.XPATH, '//table[@class="Grid"]')

    # Get all "td" elements that are children of the "table" element
    td_elements = grid_table.find_elements(By.XPATH, './/td')

    # extract the contents of the td elements into an array
    contents = []
    for td in td_elements:
      content = td.get_attribute("innerHTML")
      if len(content) > 1:
          contents.append(".")
      else:
          contents.append(content)

    # Append contents to "easy-puzzles" file
    with open(outf, 'a') as file:
      file.write("".join(contents) + "\n")


options = Options()
options.add_argument("--headless")
driver = webdriver.Firefox(options=options)
driver.implicitly_wait(3)

driver.get("http://www.enjoysudoku.com/webplay")
driver.find_element(By.XPATH, '//div[contains(text(), "Play Game")]').click()
driver.find_element(By.XPATH, '//div[text()="Easy"]').click()
print_sudoku(driver, "easy-puzzles")
driver.find_element(By.XPATH, '//div[contains(text(), "Game")]').click()
driver.find_element(By.XPATH, '//div[contains(text(), "Give Up")]').click()

driver.find_element(By.XPATH, '//div[contains(text(), "Play Game")]').click()
driver.find_element(By.XPATH, '//div[text()="Moderate"]').click()
print_sudoku(driver, "moderate-puzzles")
driver.find_element(By.XPATH, '//div[contains(text(), "Game")]').click()
driver.find_element(By.XPATH, '//div[contains(text(), "Give Up")]').click()

driver.find_element(By.XPATH, '//div[contains(text(), "Play Game")]').click()
driver.find_element(By.XPATH, '//div[text()="Intricate"]').click()
print_sudoku(driver, "hard-puzzles")
driver.find_element(By.XPATH, '//div[contains(text(), "Game")]').click()
driver.find_element(By.XPATH, '//div[contains(text(), "Give Up")]').click()

driver.find_element(By.XPATH, '//div[contains(text(), "Play Game")]').click()
driver.find_element(By.XPATH, '//div[text()="Difficult"]').click()
print_sudoku(driver, "very-hard-puzzles")
