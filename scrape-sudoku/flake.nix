{
    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs";
        utils.url = "github:numtide/flake-utils";
        poetry2nix = {
          url = "github:nix-community/poetry2nix";
          inputs.nixpkgs.follows = "nixpkgs";
        };
    };

    outputs = { self, nixpkgs, utils, poetry2nix }: (utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages."${system}";
    in {
        devShell = pkgs.mkShell {
          venvDir = "./venv";
          buildInputs = with pkgs; [
            python310Packages.venvShellHook


            geckodriver
            pre-commit
          ];
          postShellHook = ''
             export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:${pkgs.lib.makeLibraryPath [
               pkgs.stdenv.cc.cc
             ]}
          '';
        };
      }
  ));
}
