{
    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs";
        utils.url = "github:numtide/flake-utils";
        poetry2nix = {
          url = "github:nix-community/poetry2nix";
          inputs.nixpkgs.follows = "nixpkgs";
        };
    };

    outputs = { self, nixpkgs, utils, poetry2nix }: (utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages."${system}";
    in {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            poetry
          ];
          shellHook = ''
            # python -m venv venv
            if [[ ! -f "pyproject.toml" ]]; then
              poetry init -n
            fi
            # poetry env use venv/bin/python
            poetry install --no-root
            source $(poetry env info --path)/bin/activate
          '';
        };
      }
  ));
}

