import sys
from web3 import Web3
import sqlite3
import os

w3 = Web3()
con = sqlite3.connect("eth_balances.db")
cur = con.cursor()

def create_accounts(batchsize=400):
    print(".", end="")
    sys.stdout.flush()
    accounts = []
    for i in range(batchsize):
        account = w3.eth.account.create()
        accounts.append(account)
    return accounts


def query_addresses(accounts):
    address_list = ", ".join([f'"{account.address.lower().strip()}"' for account in accounts])
    # a test address: "0xef04a6eedbef3cb8c60a9d89a8e034e11c718bc2")
    query = f"""
        SELECT address, eth_balance
        FROM balances
        WHERE address IN ({address_list})
    """
    cur.execute(query)
    data = cur.fetchall()
    return data


def print_accounts(accounts):
    for account in accounts:
        print(w3.to_hex(account._private_key), "  ", account.address)


for i in range(10000):
    accounts = create_accounts()
    res = query_addresses(accounts)
    if res:
        print("\n")
        for r in res:
            print(f"Found key for {r[0]} with balance {r[1]}")
            print_accounts(accounts)
            for account in accounts:
                # stripping just in case...
                if r[0].strip() == account.address.lower().strip():
                    i = 0
                    while os.path.exists(f"found{i}.key"):
                        i += 1
                    with open(f"found{i}.key", "w") as f:
                        print(f"Created found{i}.key")
                        f.write(w3.to_hex(account._private_key))
