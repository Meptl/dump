import os
import requests
from bs4 import BeautifulSoup

save_dir = 'images'
os.makedirs(save_dir, exist_ok=True)

# Function to download an image given its URL
def download_image(url, save_directory):
    response = requests.get(url)
    filename = os.path.basename(url)
    save_path = os.path.join(save_directory, filename)
    with open(save_path, 'wb') as f:
        f.write(response.content)
    
    print(f"Image downloaded: {filename}")

response = requests.get(os.getenv('TARGET_URL'))
soup = BeautifulSoup(response.content, 'html.parser')
tables = soup.find_all('table')
for table in tables:
    rows = table.find_all('tr')[1:]  # Drop header row
    for row in rows:
        cell = row.find('td')
        url = cell.find('img')['src']
        download_image("https://wildfrostwiki.com" + url, save_dir)
