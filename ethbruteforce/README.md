# Ethbruteforce
A bunch of files that can let you brute force the private keys of ethereum
addresses.

# Setup
* Export the relevant tables from Google Big Query as a bunch of split csv files
* Use prep_data.py to convert csv into a single large sqlite database
* Use querylocal.py to start wasting CPU cycles
