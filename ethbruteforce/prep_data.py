import csv
import sqlite3
import sys

con = sqlite3.connect("eth_balances.db")
cur = con.cursor()
cur.execute("CREATE TABLE IF NOT EXISTS balances (address TEXT PRIMARY KEY, eth_balance REAL);")


with open(sys.argv[1], "r") as fin:
    dr = csv.DictReader(fin)
    for i in dr:
        cur.execute("INSERT OR IGNORE INTO balances (address, eth_balance) VALUES (?, ?);", (i["address"], i["eth_balance"]))

con.commit()
con.close()
